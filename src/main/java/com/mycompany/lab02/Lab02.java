/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab02;

import java.util.Scanner;

/**
 *
 * @author PONG
 */
public class Lab02 {

    static char[][] Table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char Player = 'X';
    static int row,col;
    static String Yes;
    static String No;
    static String A;
    
    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            
            if(isWinner()) {
                printTable();
                printWin();
                break;
            }if(CheckDraw()){
                printTable();
                System.out.println("Draw!!");
                break;
            }
            SP();
        }printRestart();
    }
    private static void printWelcome() {
        System.out.println("XO GameCommand Line");
        System.out.println("Welcome To XO Game!");
    }
    private static void printTable() {
        for(int i=0; i<3;i++){
            for(int j=0; j<3;j++){
                System.out.print(Table[i][j]+" ");
            }
            System.out.println("");
        }
    }
    private static void printTurn() {
        System.out.println(Player+ " Turn");
    }
    private static void inputRowCol() {
        Scanner c = new Scanner(System.in);
        while(true){
            System.out.print("Please input row,col:");
            row = c.nextInt();
            col = c.nextInt();
            if(Table[row-1][col-1]=='-'){
                Table[row-1][col-1] = Player;
                break;
            }
        }
    } 
    private static void SP(){
        if(Player=='X'){
            Player = 'O';
        }else{
            Player = 'X';
        }
    }
    private static boolean isWinner(){
        if(CheckRow()){
            return true;
        }if(CheckCol()){
            return true;
        }if(CheckDg1()){
            return true;
        }if(CheckDg2()){
            return true;
        }
        return false;
    }
    private static void printWin(){
        System.out.println(Player + " Win !!");
    }
    private static boolean CheckRow(){
        for(int i=0;i<3;i++){
            if(Table[row-1][i]!= Player){
                return false;
            }
        }
        return true;
    }
    private static boolean CheckCol() {
        for(int j=0;j<3;j++){
            if(Table[j][col-1]!= Player){
                return false;
            }
        }
        return true;
    }
    private static boolean CheckDraw() {
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(Table[i][j]== '-'){
                    return false;
                }
            }    
        }
        return true;
    }
    private static boolean CheckDg1() {
        for(int i=0;i<3;i++){
            if(Table[i][i]!= Player){
                return false;
            }
        }
        return true;
    }
    private static boolean CheckDg2() {
        for(int j=0;j<3;j++){
            if(Table[0+j][2-j]!= Player){
                return false;
            }
        }
        return true;
    }
        private static void printDraw() {
        System.out.println("Draw");
    }
    private static void printRestart() {
        System.out.println("Restart ? (Yes/No)");
        Scanner kb = new Scanner(System.in);
        A = kb.next();
        if(A.equals("Yes")){
                        for(int i = 0;i<3;i++){
                for(int j =0;j<3;j++){
                    Table[i][j] = '-';
                }
            }
            printWelcome();
        while(true){
        printTable();
        printTurn();
        inputRowCol(); 
        if(isWinner()){
            printTable();
            printWin();
            break;
        }else if(CheckDraw()){
            printTable();
            printDraw();
            break;
        } 
        SP(); 
    }
        printRestart();
        }
    }
}   